class_name MainScene extends Node

@export var multiscenecode_to_load: Array[PackedScene] = []

var started_background_loading = false
var currently_background_loading = false
var retain_previous_scene = false
var multiscenecode_not_loaded: Array
var last_resource_path_loaded
var completed_stages:int = 0
var total_stages:int = 0

signal loading_complete

func _process(_delta: float) -> void:
    if not started_background_loading:
        started_background_loading = true
        load_multiscenecode()
        
func goto_scene(file_path:String, retain_previous=false) -> void:
    while PM.submodules_ready == false:
        await get_tree().process_frame
    PM.LoadingScreen.progress(0.1)
    PM.LoadingScreen.fade_in()
    PM.LoadingScreen.item_name(file_path)
    retain_previous_scene = retain_previous
    PM.BGLoad.load_n_callback(file_path, self, "_finish_scene_change")
    
func _finish_scene_change(results):
    if results[0] == "finished":
        var loaded_scene = results[1].instantiate()
        if PM.artificial_delay_between_states != 0:
            await get_tree().create_timer(PM.artificial_delay_between_states).timeout
        PM.TheContainer.assign_as_next_scene(loaded_scene)
    elif results[0] == "loading":
        PM.LoadingScreen.progress(results[1] * 100.0)
    
func load_multiscenecode():
    if multiscenecode_to_load.size() > 0:
        currently_background_loading = true
        multiscenecode_not_loaded = multiscenecode_to_load.duplicate()
        loop_load_multiscenecode()
    else:
        currently_background_loading = false

func loop_load_multiscenecode():
    if multiscenecode_not_loaded.size() > 0:
        last_resource_path_loaded = multiscenecode_not_loaded.pop_front().resource_path
        PM.BGLoad.load_n_callback(last_resource_path_loaded, self, "setup_multiscenecode")
    else:
        currently_background_loading = false

func setup_multiscenecode(results):
    if results[0] == "finished":
        completed_stages += 1
        update_progress()
        var loaded_scene = results[1].instantiate()
        if not PM.MSC.has_node(NodePath(loaded_scene.name)):
            PM.MSC.add_child(loaded_scene)
        call_deferred("loop_load_multiscenecode") #Wait 1 frame for on_ready and drawing 

func update_progress(forced_val:float=-1.0):
    if forced_val == -1.0:
        var stages_percent_comp: float = 100.0
        if total_stages != 0:
            stages_percent_comp = (float(completed_stages) / float(total_stages) * 100.0)
        PM.LoadingScreen.progress(stages_percent_comp)
    else:
        PM.LoadingScreen.progress(forced_val)
