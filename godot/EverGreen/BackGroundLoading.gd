#class_name BackGroundLoading
extends Resource

var is_loading:bool = false
var __background_threads:Dictionary = {}
var __queued_threads: Array = []
var __threads_progress: Dictionary = {} 
var __testing_force_slow_threads = false
var __testing_dont_start_thread = false
var _verbose = false
# __threads_progress = {thread_num: [state, state_val, the_resource_path, 
# finished_callback_obj, finished_callback_meth, progress_callback_obj, 
# progress_callback_meth}
var __loaded_res: Dictionary = {}   # Temporary storage for resources.

func ensure_statics():
    if PM.debug_level <= 1:
        _verbose = true
    if PM.statics.has("BGL_next_thread_num"):
        return
    else:   
        PM.statics["BGL_next_thread_num"] = 0
        

func load_n_callback(the_resource_path, callback_object, callback_method, 
    callback_object_progress=null, callback_method_progress:String=""
) -> int:
    is_loading = true
    ensure_statics()
    if the_resource_path == null:
        printerr(
            "Error: BGLoad: The resource path was null, this is not allowed! "
            + "Was expected to call " + str(callback_object.name) + "." + 
            callback_method + "  | Exiting now!"
        )
        PM.get_tree().quit(ERR_FILE_BAD_PATH)
        return 0
    PM.statics.BGL_next_thread_num += 1
    if callback_method_progress == "":
        callback_object_progress = callback_object
        callback_method_progress = callback_method
    __threads_progress[PM.statics.BGL_next_thread_num] = ["queued", null, 
        the_resource_path, callback_object, callback_method, callback_object_progress,
        callback_method_progress]
    if PM.current_threads >= PM.max_threads:
        if __background_threads.size() == 0:
             __start_a_thread(PM.statics.BGL_next_thread_num, the_resource_path, 
                callback_object, callback_method, callback_object_progress,
                callback_method_progress
            )
        else:
            __queued_threads.append(PM.statics.BGL_next_thread_num)
    else:
        __start_a_thread(PM.statics.BGL_next_thread_num, the_resource_path, 
            callback_object, callback_method, callback_object_progress,
            callback_method_progress
        )
    return PM.statics.BGL_next_thread_num
    
func __start_a_thread(thread_num, the_resource_path, _callback_object, _callback_method, 
    _callback_object_progress=null, _callback_method_progress:String=""
) -> int:
    is_loading = true
    PM.current_threads += 1
    var new_thread = Thread.new()
    __background_threads[thread_num] = new_thread
    if not __testing_dont_start_thread:
        var bound_method = Callable(self, "__threaded_background_loader").bind(
            [the_resource_path, thread_num]
        )
        var start_success = new_thread.start(bound_method, Thread.PRIORITY_LOW)
        if start_success != OK:
            if _verbose:
                print_debug("Start a thread is not OK.")
            pass
        else:
            pass
    return thread_num

func reset_for_testing():
#    print("Reset for testing called!")
    var duplicant = __background_threads.duplicate()
    for thread_num in duplicant:
        if not __background_threads[thread_num].is_alive():
            #print("Background thread was not active for thread num ", thread_num)
            __background_threads.erase(thread_num)
            PM.current_threads -= 1
        else:
            pass
#            __background_threads[thread_num].wait_to_finish()
#            __background_threads.erase(thread_num)
#    print(__threads_progress)
    var wait = true
    var every_100 = 0
    while wait:
        if __threads_progress.size() == 0:
            if __background_threads.size() == 0:
                if __queued_threads.size() == 0:
                    wait = false
        every_100 += 1
        if every_100 > 99:
            every_100 = 0
            print("__threads_progress = ", __threads_progress)
            print("__background_threads = ", __background_threads)
            print("__queued_threads = ", __queued_threads)
        await PM.get_tree().process_frame
    

func _loading_progress_update(thread_num, progress) -> void:
    if not __threads_progress.has(thread_num):
        return
    if __threads_progress[thread_num][0] == "finished":
        return
    if __threads_progress[thread_num][1] != null:
        if __threads_progress[thread_num][1] > progress:
            return
    __threads_progress[thread_num][0] = "loading"
    __threads_progress[thread_num][1] = progress
    handle_progress_update(thread_num, "loading", progress)

func _finished_background_loading(thread_num) -> void:
    var obj = __threads_progress[thread_num][3]
    var meth = __threads_progress[thread_num][4]
    if not __testing_dont_start_thread:
        if __loaded_res.has(thread_num):
            obj.call(meth, ["finished", __loaded_res[thread_num]])
        else:
            printerr(
                "Error: BGL: _finished_background_loading: __loaded_res" +
                " does not have " + str(thread_num)
            )
    __threads_progress.erase(thread_num)
    if not __testing_dont_start_thread:
        __loaded_res.erase(thread_num)
    
func _failed_background_loading(thread_num, error_num, the_resource_path) -> void:
    var err_msg = ("BackGroundLoading: A background thread, thread number " + 
        str(thread_num) + ", failed to load resource:'" + the_resource_path + 
        "' with " + "an error number of " + str(error_num) + ".")
    printerr(err_msg)
    PM.TheContainer.current_scene.get_child(0).goto_scene("res://scenes/TheContainer/FailedToLoadSceneError.tscn")
    #PM.get_tree().quit(ERR_CANT_ACQUIRE_RESOURCE)

func handle_progress_update(thread_num, state, state_val) -> void:
    if __threads_progress.has(thread_num):
        if state == "loading":
            if __threads_progress[thread_num][0] != "loading":
                var obj = __threads_progress[thread_num][5]
                var meth = __threads_progress[thread_num][6]
                if state_val > 0.99:
                    state_val = 0.99
                obj.call(meth, [state, state_val])
        if _verbose:
            print("BGL: Progress Update: Thread #", thread_num, "  state=", 
                state, "  progress=", state_val)

func move_resource_from_background_to_main(thread_num):
    if _verbose:
        print("move_resource_from_background_to_main(", thread_num, ")")
    if __threads_progress.has(thread_num):
        __threads_progress[thread_num][0] = "loading"
        __threads_progress[thread_num][1] = 0.99
        var obj = __threads_progress[thread_num][5]
        var meth = __threads_progress[thread_num][6]
        if is_instance_valid(obj):
            obj.call(meth, ["loading", 0.99])
            if __background_threads.has(thread_num):
                if __background_threads[thread_num].is_started():
                    __loaded_res[thread_num] = __background_threads[thread_num].wait_to_finish()
                else:
                    PM.Log("BackGroundLoading: Error: " + 
                        "Background thread was not started " + str(thread_num) +
                        ":  " + str(__threads_progress[thread_num]), "error"
                    ) 
            else:
                if not __testing_dont_start_thread:
                   PM.Log("BackGroundLoading: Error: " + 
                        "Background threads does not have " + str(thread_num) +
                        ":  " + str(__threads_progress[thread_num]), "error"
                    )
            tear_down_thread(thread_num)
            _finished_background_loading(thread_num)
        else:
            PM.Log("BackGroundLoading: Error: " + 
                "Main Thread Object has been freed. " + " : object = " +
                str(__threads_progress[thread_num][5]) + " : method = " +
                str(__threads_progress[thread_num][6]) + " : " + str(thread_num)
                + ":  " + str(__threads_progress[thread_num]), "error"
            )
    
func tear_down_thread(thread_num) -> void:
    var __ = __background_threads.erase(thread_num)
    PM.current_threads -= 1
    if __queued_threads.size() > 0:
        if PM.current_threads >= PM.max_threads:
            if __background_threads.size() == 0:
                __start_a_queued_thread()
        else:
            __start_a_queued_thread()

func __start_a_queued_thread():
    var next = __queued_threads.pop_front()
    var the_resource_path = __threads_progress[next][2]
    var callback_object = __threads_progress[next][3]
    var callback_method = __threads_progress[next][4] 
    var callback_object_progress = __threads_progress[next][5]
    var callback_method_progress = __threads_progress[next][6]
    __start_a_thread(next, the_resource_path, callback_object, 
        callback_method, callback_object_progress, 
        callback_method_progress
    )

func __threaded_background_loader(data) -> Resource:
    var the_resource_path = data[0]
    var my_num = data[1]
    var progress = 0.01
    call_deferred("_loading_progress_update", my_num, progress)
    var internal = true
    if not "res://" in the_resource_path:
        internal = false
    if internal:
        if ".json" in the_resource_path:
            return __internal_json_loader(my_num, the_resource_path, progress) 
#        elif ".txt" in the_resource_path or ".md" in the_resource_path:
#            pass  # Unsupported right now.
        else:
            var tmp = __internal_resource_loader(my_num, the_resource_path, progress)
            return tmp
    else:
        call_deferred("_failed_background_loading", my_num, 
            ERR_FILE_UNRECOGNIZED, the_resource_path
        )
        return null
        
func __internal_resource_loader(my_num, the_resource_path, progress) -> Resource:
    var start_time = Time.get_ticks_msec()
    var loader = ResourceLoader.load_threaded_request(the_resource_path)
    if loader != OK:
        call_deferred("_failed_background_loading", my_num, 
            loader, the_resource_path
        )
    var err = ResourceLoader.THREAD_LOAD_IN_PROGRESS
    while err == ResourceLoader.THREAD_LOAD_IN_PROGRESS:
        var prog_array = []
        err = ResourceLoader.load_threaded_get_status(the_resource_path, prog_array)
        if err == ResourceLoader.THREAD_LOAD_IN_PROGRESS:
            progress = float(prog_array[0])
        else:
            progress=0.99       
        call_deferred("_loading_progress_update", my_num, progress)
    if err == ResourceLoader.THREAD_LOAD_LOADED:
        var resource = ResourceLoader.load_threaded_get(the_resource_path)
        if __testing_force_slow_threads:
            var current_time = Time.get_ticks_msec()
            while start_time + 20 > current_time:
                OS.delay_msec(1)
                current_time = Time.get_ticks_msec()
        if not __testing_dont_start_thread:
            call_deferred("move_resource_from_background_to_main", my_num)
        return resource
    else:
        call_deferred("_failed_background_loading", my_num, err, the_resource_path)
        return null

func __internal_json_loader(my_num, the_resource_path, _progress):
    if FileAccess.file_exists(the_resource_path):
        var file = FileAccess.open(the_resource_path, FileAccess.READ)
        var text = file.get_as_text()
        var test_json_conv = JSON.new()
        test_json_conv.parse(text)
        var temp = test_json_conv.get_data()
        call_deferred("move_resource_from_background_to_main", my_num)
        return temp
    else:
        call_deferred("_failed_background_loading", my_num, 
            ERR_FILE_NOT_FOUND, the_resource_path
        )
